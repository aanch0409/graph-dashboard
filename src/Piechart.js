import React, { Component } from 'react';
import { scaleOrdinal } from 'd3-scale';
import { arc as d3Arc, pie as d3Pie } from 'd3-shape';
import { LabeledArc } from './Arc';

const Piechart = ({x, y, innerRadius, outerRadius, data}) => {
    let r;
    if(innerRadius<outerRadius)
    {
        r=(innerRadius/2);
    }
    else
    {   	
        r=(outerRadius/2);
    }
    
    let pie = d3Pie()
                .value((d) => d.value)(data),        
        translate = `translate(${x+r}, ${y+r})`,
        colors = scaleOrdinal().range([
                      '#98abc5',
                      '#8a89a6',
                      '#7b6888',
                      '#6b486b',
                      '#a05d56',
                      '#d0743c',
                      '#ff8c00',
                    ]);
 
    return (
        <g transform={translate}>
            {pie.map((d, i) => (
                <LabeledArc key={`arc-${i}`}
                            data={d}
                            innerRadius={innerRadius}
                            outerRadius={outerRadius}
                            color={colors(i)} />))}
        </g>
    );
};

export default Piechart;
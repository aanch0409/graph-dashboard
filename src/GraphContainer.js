import React from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import Piechart from './Piechart'
import LineChart from './LineChart';
import { withStyles } from '@material-ui/core/styles';
import ReactDOM from 'react-dom';
const GridContainer = WidthProvider(RGL);

const graphstyles = theme => ({
  graphOuterContainer : {
      display: "inline-block",
      width: "96%",
      height: "82%",
      //"flex-wrap": "nowrap",
      "background-color": "#F6F6F6",
      "margin-left": "2%",
      "margin-top": "1%",
  },
graphtopheaderclass : {
      display: "inline-block",
      width: "100%",
      height: "4%",
      "border-bottom" : "1px solid"
  },
  graphContainerclass : {
      display: "inline-block",
      width: "100%",
      height: "92%",
      "border-bottom" : "1px solid"
  },
  "react-resizable-handle" :
    {    
        position: "absolute",
        width: "4px",
        height: "4px",
        bottom: "0",
        right: "0",
        cursor : "se-resize"
    } 
});


class GraphContainer extends React.PureComponent {
  static defaultProps = {
    isDraggable: true,
    isResizable: true,
    items: 2,
    rowHeight: 100,
   // onLayoutChange: function() {},
    cols: 50
  };

   constructor(props)
    {
        super(props)        
        this.state = {
            layout : [
              {i: 'a', x: 0, y: 0, w: 20, h: 4,Name : "Line Chart"},
              {i: 'b', x: 0, y: 0, w: 20, h: 4,Name : "Pie Chart"}
            ],
            data : [
               {a: 1, b: 3},
               {a: 2, b: 6},
               {a: 3, b: 2},
               {a: 4, b: 12},
               {a: 5, b: 8}
             ],
        }
    }

componentDidMount()
  {
      let data = this.state.data
      let width = this.refdiv.getBoundingClientRect().width
      let height = this.refdiv.getBoundingClientRect().height
      
      let ElementDiv = document.getElementById('a').getElementsByTagName('svg')[0];
      let ElementsecondDiv = document.getElementById('b').getElementsByTagName('svg')[0];
      
      ReactDOM.render(<LineChart data={data} width={width} height={height}/>, document.getElementById('a').getElementsByTagName('svg')[0]);    
      
      ReactDOM.render(<Piechart x={240} y={180} outerRadius={150} innerRadius={50}
                  data={[{value: 92-34, label: 'Learning Disability'},
                         {value: 34, label: 'Sleep Problems'}]} />, document.getElementById('b').getElementsByTagName('svg')[0]);
     // ElementDiv.append('<LineChart data={data} width={width} height={height}/>')
  }
 
  generateDOM = () =>{
    const { classes } = this.props;
    let data = this.state.data
    let _this = this
    return this.state.layout.map(function(l) {
      return (
        <div key={l.i} data-grid={l} ref={el => _this.refdiv = el}>
            <div className = {classes.graphtopheaderclass} > {l.Name}
            </div>
            <div className = {classes.graphContainerclass} id = {l.i}>
                  <svg width="100%" height="100%">
                  </svg>
            </div>
        </div>
      );
    });
  }

  onResize(layout, oldLayoutItem, layoutItem, placeholder) {

  }

  render() {
       const { classes } = this.props;
    return (
    <div className = {classes.graphOuterContainer}>
      <GridContainer
        onResize={this.onResize}
        {...this.props}
      >
        {this.generateDOM()}
      </GridContainer>
    </div>
    );
  }
}

export default withStyles(graphstyles)(GraphContainer);

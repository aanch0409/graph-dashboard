import React from "react";
import GraphContainer from './GraphContainer'
import { withStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import injectSheet from 'react-jss'

const styles = theme => ({
  dashboardContainer : {
      width: "100%",
      height: "100%",
      display: "inline-block",
      "background-color": "#F6F6F6",
    //  "flex-wrap" : "wrap",
    //  "flex-direction" : "column"
      float : "left"
  },
  dashboardHeaderDiv : {
      display : "inline-block",
      width : "96%",
      height : "8%",
     // "flex-wrap" : "nowrap",
      "background-color": "#F6F6F6",
      //"box-shadow" : "3px 5px 2px 1px #ccc5c5",
      "margin-bottom" : "2.5px",
      "border-bottom" : "1px solid #e2dede",
      "margin-left": "2%",
       "margin-top" : "0.5%",
      float : "left"
  },
  headerTextDiv : {
      flex : 1
  },
  headerSettingsDiv : {
      flex : 2,
      display : "none",
      "flex-direction" : "row-reverse",
      //"padding-right": "3%",
      "padding-top": "1%"
  },
  headerDashobardDiv : {
    color: "#455360",
    "margin-top": "1%",
    "font-size": "1.5em",
    //"margin-left": "4%"
  },
 headerDashobardIconDiv : {
  },
 headerSettingsIcon : {
     fontSize: 26,
     color: "#455360",
     "margin-left": "1%"
  },
 headerAddIcon : {
     fontSize: 26,
     color: "#455360"
  }
});


class Dashboard extends React.PureComponent {
    
    render()
    {
        const { classes } = this.props;
        return(
            <div className = {classes.dashboardContainer}>
                <div className= {classes.dashboardHeaderDiv}>
                    <div className = {classes.headerTextDiv}>
                        <div className = {classes.headerDashobardDiv}> Dashboard
                        </div>
                    </div>
                    <div className = {classes.headerSettingsDiv}>
                              <Icon className = {classes.headerSettingsIcon}>
                                    settings
                              </Icon>
                              <Icon className = {classes.headerAddIcon}>
                                    add_circle
                              </Icon>
                    </div>
                </div>
                <GraphContainer/>
            </div>            
        )        
    }
}

export default withStyles(styles)(Dashboard)
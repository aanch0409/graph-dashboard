import React from 'react';
import { scaleLinear } from 'd3-scale';
import { arc as d3Arc, line as d3line } from 'd3-shape';
import * as d3 from 'd3'
import { withStyles } from '@material-ui/core/styles';

const graphstyles = theme => ({
    "path" :
    {
      "stroke" : "steelblue",
      "stroke-width": "2px",
      "fill" : "none"
    },
    "axis" :
    {
      "stroke" : "#000"
    },
    "axis-labels text" :
    {
      "fill" : "#000",
      "fill-opacity": "0.9",
      "font-size": "12px",
      "text-anchor": "middle"
    },
    "axis-labels line" :
    {
      "stroke" : "#000"
    },
    "gridline" :
    {
      opacity: "0.2"
    },
    "outerdiv" :
    {
        width : "100%",
        height : "100%"
    }
});

class LineChart extends React.Component { 
    
  render() {
        const { classes } = this.props; 
    const {data, width, height , element} = this.props
    
    const margin = 30

    const h = height - 2 * margin, w = width - 2 * margin
    
    //const h = height , w = width 
    
    //number formatter
    const xFormat = d3.format('.2')
    
    //x scale
    const x = scaleLinear()
      .domain(d3.extent(data, d => d.a)) //domain: [min,max] of a
      .range([margin, w])
    
    //y scale
    const y = scaleLinear()
      .domain([0, d3.max(data, d => d.b)]) // domain [0,max] of b (start from 0)
      .range([h, margin])
    
    //line generator: each point is [x(d.a), y(d.b)] where d is a row in data
    // and x, y are scales (e.g. x(10) returns pixel value of 10 scaled by x)
    
    const line = d3.line()
      .x(d => x(d.a))
      .y(d => y(d.b))
 //     .curve(d3.curveCatmullRom.alpha(0.5)) //curve line
    
    var bisectDate = d3.bisector(function(d) { return d.a; }).left;
    
    var svg = d3.select("#" + element +" svg")
    
    var g = svg.append("g")
    .attr("transform", "translate(" + margin + "," + 0 + ")");
    
    var focus = g.append("g")
        .attr("class", "focus")
        .style("display", "none");

    focus.append("line")
        .attr("class", "x-hover-line hover-line")
        .attr("y1", 0)
        .attr("y2", height-margin);

    focus.append("circle")
        .attr("r", 7.5);

    focus.append("text")
        .attr("x", 15)
      	.attr("dy", ".31em");
  
    focus.append("path")
    .attr("d", "M 17,15 50,37.5 35,52.5 z M37,40 82,90 z")
    .attr("class","arrow");

    svg.append("rect")
        .attr("transform", "translate("+ margin +"," + margin + ")")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", function() { focus.style("display", null); })
        .on("mouseout", function() { focus.style("display", "none"); })
        .on("mousemove", mousemove);

    function mousemove() {
            var x0 = x.invert(d3.mouse(this)[0]),
          i = bisectDate(data, x0, 1),
          d0 = data[i - 1],
          d1 = data[i],
          d = x0 - d0.a > d1.a - x0 ? d1 : d0;
          var x1 = x(d.a) - 30;
      focus.attr("transform", "translate(" + x1 + "," + y(d.b) + ")");
      focus.select("text").text(function() { return d.b; });
      focus.select(".x-hover-line").attr("y2", height - y(d.b));
      focus.select(".y-hover-line").attr("x2", width + width);
    }
    
  let xmargin = 0-margin;
  let ymargin = height
   g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(" + xmargin + "," + ymargin + ")")
        .call(d3.axisBottom(x));

    g.append("g")
        .attr("class", "axis axis--y")
        .call(d3.axisLeft(y).ticks(6).tickFormat(function(d) { return parseInt(d); }))
        .append("text")
        .attr("class", "axis-title")
        .attr("transform", "rotate(-90)")
        .attr("transform", "translate(" + xmargin + "," + ymargin + ")")
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .attr("fill", "#5D6971")
        .text("Population)");
      
    /*const xTicks = x.ticks(6).map(d => (
        x(d) > margin && x(d) < w ? 
          <g transform={`translate(${x(d)},${h+10})`}>  
            <line x1='0' x1='0' y1='0' y2={h} transform="translate(0,-5)"/>
          </g>
        : null
    ))

    const yTicks = y.ticks(6).map(d => (
        y(d) > 10 && y(d) < h ? 
          <g transform={`translate(0,${y(d)})`}>  
            <text x="-12" y="5">{xFormat(d)}</text>
            <line x1='0' x1='0' y1='0' y2='0' transform="translate(-5,0)"/>
            <line className='gridline' x1='0' x1={5} y1='0' y2='0' transform="translate(-5,0)"/> 
          </g>
        : null
    ))*/

    return  (
        <React.Fragment>
             <line className="axis" x1={margin} x2={w} y1={h} y2={h}/>
             <line className="axis" x1={margin} x2={margin} y1={margin} y2={h}/>
             <path d={line(data)}/>
        </React.Fragment>
    )
  }
}

LineChart.defaultProps = {
	marginTop: 10,
	marginLeft: 30,
	marginBottom: 30,
	marginRight: 30,
	width: 960,
	height: 500,
	x: d => d.x,
	y: d => d.y,
	r: d => 3,
	fill: d => "#000",
	stroke: d => "none",
	xTickArguments: [],
	yTickArguments: []
};


export default withStyles(graphstyles)(LineChart);;
